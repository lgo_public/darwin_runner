On a fresh new mac run the following commands. We may need to run
it twice if xcode is not installed on the first run:

```
git clone https://gitlab.com/lgo_public/darwin_runner.git
cd darwin_runner
./install_runner
```
